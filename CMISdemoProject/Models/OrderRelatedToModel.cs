﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CMISdemoProject.Models
{
    public class OrderRelatedToModel
    {
        public long? Id { get; set; }
        public long? OrderEntryID { get; set; }
        public int? RelatedTo { get; set; }
        public string paragraph { get; set; }
        public string Description { get; set; }

    }
}