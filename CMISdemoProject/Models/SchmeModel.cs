﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CMISdemoProject.Models
{
    public class SchmeModel
    {
        public long Id { get; set; }
        public string SchemeNameHin { get; set; }
        public string SchemeNameEng { get; set; }
        public string Logo { get; set; }
        public string Description { get; set; }
        public string FileAttachment { get; set; }
        public string MadeOfApplication { get; set; }
        public string MadeOfApplicationtxt { get; set; }
        public string CategoryOfScheme { get; set; }
        public string BeneficiaryType { get; set; }
        public string IsRelativeWithOrder { get; set; }
        public string NoOfEffectivePeople { get; set; }
        public string SchemeForms { get; set; }
        public string ListOfRequiredDocs { get; set; }
        public string ChargabaleScheme { get; set; }
        public string ModeOfPayment { get; set; }
        public string PaymentType { get; set; }
        public string TypeOfScheme { get; set; }
        public string SchemeTitle { get; set; }
        public string ExecutiveDepartment { get; set; }
        public string ConcernedDepartment { get; set; }
        public string HelplineNo { get; set; }
        public string Nodal { get; set; }
        public string Where { get; set; }
        public string StateLevel { get; set; }
        public DateTime DeliveryTime { get; set; }
        public string PaymentOperation { get; set; }
        public string SchemeExpritation { get; set; }
        public string EndLevelDepartment { get; set; }
        public bool isDelete { get; set; }
      
    }

}