﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CMISdemoProject.Models
{
    public class OrderEntryModel
    {
       
        public long Id { get; set; }
        public int? OrderType { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public int? Department { get; set; }
        public int? DepartmentAffected { get; set; }
        public string ReferencesLink { get; set; }
        public string TitleIsPublicOrCUG { get; set; }
        public string TitleIsGOROrGOI { get; set; }
        public string OrderNo { get; set; }
        public long? RelatedOrderNo { get; set; }
        public int? RelatedScheme { get; set; }
        public int? Category { get; set; }
        public string SearchText { get; set; }
        public int? Sector { get; set; }
        public string DocumentUrl { get; set; }
        public DateTime? DateOfEntry { get; set; }
        public DateTime? DateofIssue { get; set; }
        public DateTime? OrderDate { get; set; }

        public  List<OrderRelatedToModel> RelatedToOrderList { get; set; }
    }

    }