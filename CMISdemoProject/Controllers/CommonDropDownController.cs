﻿using AutoMapper;
using CMISdemoProject.Data;
using CMISdemoProject.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.Net.Http.Headers;
using System.Web.Helpers;
using Newtonsoft.Json;
using System.IO;


namespace CMISdemoProject.Controllers
{
    public class CommonDropDownController : ApiController
    {
		[HttpGet]
		public IDictionary<string, object> AllDropDown(string keys)
		{
			IDictionary<string, object> Data = new Dictionary<string, object>();
            try
            {
                if (!string.IsNullOrEmpty(keys))
                {
                    foreach (var item in keys.Split(','))
                    {
                        var temp = Data.Where(x => x.Key == item).FirstOrDefault().Key;
                        if (temp == null)
                        {
                            if (item.Equals(DdlKeys.ddlEntry.ToString()))
                            {
                                List<CommonDDlModel> obj = GetEntryList();
                                Data.Add(item, obj);
                            }

                            if (item.Equals(DdlKeys.ddlCategoryScheme.ToString()))
                            {
                                List<CommonDDlModel> obj = GetCategoryScheme();
                                Data.Add(item, obj);
                            }
                            if (item.Equals(DdlKeys.ddlDepartment.ToString()))
                            {
                                List<CommonDDlModel> obj = GetDepartmentList();
                                Data.Add(item, obj);
                            }

                            if (item.Equals(DdlKeys.ddlTypeScheme.ToString()))
                            {
                                List<CommonDDlModel> obj = GetTypeScheme();
                                Data.Add(item, obj);
                            }

                            if (item.Equals(DdlKeys.ddlNodel.ToString()))
                            {
                                List<CommonDDlModel> obj = GetNodel();
                                Data.Add(item, obj);
                            }
                            if (item.Equals(DdlKeys.ddlConcernedDepartment.ToString()))
                            {
                                List<CommonDDlModel> obj = GetConcernedDepartment();
                                Data.Add(item, obj);
                            }
                            if (item.Equals(DdlKeys.ddlExecutiveDepartment.ToString()))
                            {
                                List<CommonDDlModel> obj = GetExecutiveDepartment();
                                Data.Add(item, obj);
                            }
                            if (item.Equals(DdlKeys.ddlBeneficiarytype.ToString()))
                            {
                                List<CommonDDlModel> obj = GetBeneficiarytype();
                                Data.Add(item, obj);
                            }
                        }
                    }
                }
            }
            catch (Exception e)
            {
                throw;
            }

			return Data.Count > 0 ? Data : null;
		}

		public enum DdlKeys
		{
            ddlEntry, ddlDepartment, ddlBeneficiarytype, ddlExecutiveDepartment, ddlConcernedDepartment, ddlNodel, ddlTypeScheme, ddlCategoryScheme
        }


		public List<CommonDDlModel> GetEntryList()
		{
			try
			{
				using (CMOAppTestEntities db = new CMOAppTestEntities())
				{
                    List<CommonDDlModel> obj = db.TblOrderEntryTests.Select(z => new CommonDDlModel
                    {
                        Value = z.Id.ToString(),
						Name = z.Title
					}).ToList();

					return obj;
				}
			}
			catch (Exception e)
			{

				throw;
			}
		}

        public List<CommonDDlModel> GetCategoryScheme()
        {
            try
            {

                List<CommonDDlModel> data = new List<CommonDDlModel>();
                data.Add(new CommonDDlModel { Name = "Youth", Value = "1" });
                data.Add(new CommonDDlModel { Name = "Farmar", Value = "2" });

                return data;
            }
            catch (Exception)
            {

                throw;
            }
        }
        public List<CommonDDlModel> GetBeneficiarytype()
        {
            try
            {


                List<CommonDDlModel> data = new List<CommonDDlModel>();
                data.Add(new CommonDDlModel { Name = "Beneficiarytype1", Value = "1" });
                data.Add(new CommonDDlModel { Name = "Beneficiarytype2", Value = "2" });
                return data;

            }
            catch (Exception)
            {

                throw;
            }
        }
        public List<CommonDDlModel> GetExecutiveDepartment()
        {
            try
            {

                List<CommonDDlModel> data = new List<CommonDDlModel>();
                data.Add(new CommonDDlModel { Name = "ExecutiveDepartment1", Value = "1" });
                data.Add(new CommonDDlModel { Name = "ExecutiveDepartment2", Value = "2" });

                return data;
            }
            catch (Exception)
            {

                throw;
            }
        }
        public List<CommonDDlModel> GetConcernedDepartment()
        {
            try
            {
                // var data = [{ "Value":1, Name: "One" },{ "Value":2, Name: "Two"} ];
                List<CommonDDlModel> data = new List<CommonDDlModel>();
                data.Add(new CommonDDlModel { Name = "GetConcernedDepartment1", Value = "1" });
                data.Add(new CommonDDlModel { Name = "GetConcernedDepartment2", Value = "2" });
                return data;
            }
            catch (Exception)
            {

                throw;
            }
        }
        public List<CommonDDlModel> GetNodel()
        {
            try
            {

                List<CommonDDlModel> data = new List<CommonDDlModel>();
                data.Add(new CommonDDlModel { Name = "Nodel1", Value = "1" });
                data.Add(new CommonDDlModel { Name = "Nodel2", Value = "2" });
                return data;
            }
            catch (Exception)
            {

                throw;
            }
        }
        public List<CommonDDlModel> GetTypeScheme()
        {
            try
            {
                List<CommonDDlModel> data = new List<CommonDDlModel>();
                data.Add(new CommonDDlModel { Name = "TypeScheme1", Value = "1" });
                data.Add(new CommonDDlModel { Name = "TypeScheme2", Value = "2" });
                return data;
            }
            catch (Exception)
            {

                throw;
            }
        }

        public List<CommonDDlModel> GetDepartmentList()
		{
			try
			{
				using (CMOAppTestEntities db = new CMOAppTestEntities())
				{
					List<CommonDDlModel> obj = db.tblDepartmentMasters.Select(z => new CommonDDlModel
					{
						Value = z.DepartmentId.ToString(),
						Name = z.DepartmentTitle
					}).ToList();

					return obj;
				}
			}
			catch (Exception)
			{

				throw;
			}
		}
	}
}
