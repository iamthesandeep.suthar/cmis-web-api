﻿using CMISdemoProject.Data;
using CMISdemoProject.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;

namespace CMISdemoProject.Controllers
{
    public class OrderEntryController : ApiController
    {
        [HttpGet]
        public List<CommonDDlModel> GetRelatedToOrder()
        {
            try
            {
                using (CMOAppTestEntities db = new CMOAppTestEntities())
                {
                    List<CommonDDlModel> obj = db.TblOrderEntryTests.Select(z => new CommonDDlModel
                    {
                        Value = z.Id.ToString(),
                        Name = z.Title
                    }).ToList();

                    return obj.Count() > 0 ? obj : null;
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        [HttpPost]
        public bool CreateOrderEntry()
        {
            try
            {
                string FileName = string.Empty;
                string path = string.Empty;
                if (HttpContext.Current.Request.Files.Count > 0)
                {
                    var file = HttpContext.Current.Request.Files[0];
                    string extension = System.IO.Path.GetExtension(file.FileName);
                    string guidName = Guid.NewGuid().ToString();
                    FileName = guidName + extension;
                    path = System.Web.HttpContext.Current.Server.MapPath("~/Content/UploadFolder/OrderEntry/") + FileName;
                    file.SaveAs(path);
                }

                OrderEntryModel model = JsonConvert.DeserializeObject<OrderEntryModel>(HttpContext.Current.Request.Form["Data"]);
                if (model != null)
                {
                    using (CMOAppTestEntities db = new CMOAppTestEntities())
                    {
                        TblOrderEntryTest obj = new TblOrderEntryTest();
                        obj.OrderDate = model.OrderDate;
                        obj.OrderNo = model.OrderNo;
                        obj.OrderType = model.OrderType;
                        obj.ReferencesLink = model.ReferencesLink;
                        obj.RelatedOrderNo = model.RelatedOrderNo;
                        obj.RelatedScheme = model.RelatedScheme;
                        obj.SearchText = model.SearchText;
                        obj.Sector = model.Sector;
                        obj.Title = model.Title;
                        obj.Category = model.Category;
                        obj.DateOfEntry = model.DateOfEntry;
                        obj.DateofIssue = model.DateofIssue;
                        obj.Department = model.Department;
                        obj.DepartmentAffected = model.DepartmentAffected;
                        obj.Description = model.Description;
                        obj.TitleIsGOROrGOI = model.TitleIsGOROrGOI;
                        obj.TitleIsPublicOrCUG = model.TitleIsPublicOrCUG;
                        obj.DocumentUrl = FileName;

                        db.TblOrderEntryTests.Add(obj);
                        db.SaveChanges();

                        if (model.RelatedToOrderList != null && model.RelatedToOrderList.Count > 0)
                        {
                            foreach (var item in model.RelatedToOrderList)
                            {
                                if (!string.IsNullOrEmpty(Convert.ToString(item.RelatedTo)) || !string.IsNullOrEmpty(item.paragraph) || !string.IsNullOrEmpty(item.Description))
                                {
                                    TblOrderRelatedToMappingTest objRelated = new TblOrderRelatedToMappingTest();
                                    objRelated.Description = item.Description;
                                    objRelated.OrderEntryID = obj.Id;
                                    objRelated.paragraph = item.paragraph;
                                    objRelated.RelatedTo = item.RelatedTo;
                                    db.TblOrderRelatedToMappingTests.Add(objRelated);
                                }
                            }
                            db.SaveChanges();
                        }

                        return obj != null ? true : false;
                    }

                }
                return false;
            }
            catch (Exception)
            {
                return false;
            }
        }

    }
}
