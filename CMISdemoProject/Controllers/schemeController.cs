﻿using CMISdemoProject.Data;
using CMISdemoProject.Models;
using Newtonsoft.Json;
using System;
using System.IO;
using System.Web;
using System.Web.Http;

namespace CMISdemoProject.Controllers
{
    public class schemeController : ApiController
    {
        [HttpPost]
        public bool CreateScheme()
        {
            try
            {
                SchmeModel model = JsonConvert.DeserializeObject<SchmeModel>(HttpContext.Current.Request.Form["data"]);
                if (HttpContext.Current.Request.Files.Count > 0)
                {
                    foreach (var item in HttpContext.Current.Request.Files.AllKeys)
                    {
                        HttpPostedFile file = HttpContext.Current.Request.Files[item];
                        string extension = Path.GetExtension(file.FileName);
                        string guidName = Guid.NewGuid().ToString();
                        string FileName = guidName + extension;
                        string path = HttpContext.Current.Server.MapPath("~/Content/UploadFolder/SchmeEntry/") + FileName;
                        if (item.ToLower().Equals("fileattachment"))
                        {
                            model.FileAttachment = FileName;
                        }
                        else if (item.ToLower().Equals("logo"))
                        {
                            model.Logo = FileName;
                        }
                        else if (item.ToLower().Equals("schemeforms"))
                        {
                            model.SchemeForms = FileName;
                        }
                        file.SaveAs(path);
                    }
                }
                using (CMOAppTestEntities db = new CMOAppTestEntities())
                {
                    tblScheme obj = new tblScheme();
                    obj.SchemeNameHindi = model.SchemeNameHin;
                    obj.SchemeNameEnglish = model.SchemeNameEng;
                    obj.Logo = model.Logo;
                    obj.Description = model.Description;
                    obj.FileAttachment = model.FileAttachment;
                    obj.MadeOfApplication = model.MadeOfApplication;
                    obj.MadeOfApplicationtxt = model.MadeOfApplicationtxt;
                    obj.categoryOfScheme = string.IsNullOrEmpty(model.CategoryOfScheme) ? 0 : Convert.ToInt32(model.CategoryOfScheme);
                    obj.BeneficiaryType = string.IsNullOrEmpty(model.BeneficiaryType) ? 0 : Convert.ToInt32(model.BeneficiaryType);
                    obj.IsRelativeWithOrder = model.IsRelativeWithOrder;
                    obj.NoOfEffectivePeople = string.IsNullOrEmpty(model.NoOfEffectivePeople) ? 0 : Convert.ToInt32(model.NoOfEffectivePeople);
                    obj.SchemeForms = model.SchemeForms;
                    obj.ListOfRequiredDocs = model.ListOfRequiredDocs;
                    obj.ChargabaleScheme = model.ChargabaleScheme;
                    obj.ModeOfPayment = model.ModeOfPayment;
                    obj.PaymentType = model.PaymentType;
                    obj.TypeOfScheme = string.IsNullOrEmpty(model.TypeOfScheme) ? 0 : Convert.ToInt32(model.TypeOfScheme);
                    obj.SchemeTitle = model.SchemeTitle;
                    obj.ExecutiveDepartment = string.IsNullOrEmpty(model.ExecutiveDepartment) ? 0 : Convert.ToInt32(model.ExecutiveDepartment);
                    obj.ConcernedDepartment = string.IsNullOrEmpty(model.ConcernedDepartment) ? 0 : Convert.ToInt32(model.ConcernedDepartment);
                    obj.HelplineNo = string.IsNullOrEmpty(model.HelplineNo) ? 0 : Convert.ToInt32(model.HelplineNo);
                    obj.Nodal = string.IsNullOrEmpty(model.HelplineNo) ? 0 : Convert.ToInt32(model.HelplineNo);
                    obj.Where = model.Where;
                    obj.StateLevel = model.StateLevel;
                    obj.DeliveryTime = model.DeliveryTime.Equals(DateTime.MinValue)? DateTime.Now.AddYears(-100): model.DeliveryTime;
                    obj.PaymentOperation = model.PaymentOperation;
                    obj.SchemeExpritation = model.SchemeExpritation;
                    obj.EndLevelDepartment = model.EndLevelDepartment;
                    obj.isDelete = false;
                    db.tblSchemes.Add(obj);
                    db.SaveChanges();
                    return obj != null ? true : false;
                }
            }
            catch
            {
                return false;
            }
        }
    }
}
